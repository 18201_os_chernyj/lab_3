#include <stdio.h>

#define FILE_NAME "file"

int main()
{
    FILE* file;
    printf("User ID: %d\n", getuid());
    printf("Effective User ID: %d\n", geteuid());

    file = fopen(FILE_NAME, "rw");
    if (file == NULL)
    {
        perror("ERROR: fopen()");
    }
    else
    {
        fclose(file);
    }

    if (setuid(getuid()) == -1)
    {
        perror("ERROR: setuid()");
        return -1;
    }

    printf("User ID: %d\n", getuid());
    printf("Effective User ID: %d\n", geteuid());

    file = fopen(FILE_NAME, "rw");
    if (file == NULL)
    {
        perror("ERROR: fopen()");
    }
    else
    {
        fclose(file);
    }

}
